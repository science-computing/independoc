from setuptools import setup

setup(
    name='independoc',
    version='0.0.1',
    description='Static documentation generator for externally supported docs',
    url='',
    author='Mirko Vucicevich',
    author_email='mvucicev@uwaterloo.ca',
    license='Unlicense',
    packages=['independoc'],
    install_requires=[
        'markdown',
        'unidecode',
        'jinja2',
        'requests',
        'bs4',
        'bleach',
        'google-api-python-client',
        'oauth2client',
        'html5lib',
    ],
    test_suite='nose.collector',
    tests_require=['nose'],
    zip_safe=False
)
