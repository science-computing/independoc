Independoc 
============

Have you ever written an application without any useful documentations for users?

Have you ever had amazing, wonderful users go ahead and generate documentation for you?

In many cases, this user-generated documentation is either:
- domain specific (perfect for local deployment but not generalized enough to be packaged with the product)
- not tech-friendly (stored on websites, in shared folders in non-ideal formats for packaging / deployment)

If you're in this situation, **boy do I have a project for you!**

## External Documentation Collection

Independoc's intent is to (with collaboration with aforementioned amazing document-generating users) collect, parse, and generate a static documentation site within the existing static content of your site. It's really just a rudimentary static-site-generator that reads websites and google docs instead of markdown files. 

## Installation
This project isn't currently avaialble on PYPI but you can still install it through pip via
```
pip install -e git+https://git.uwaterloo.ca/science-computing/independoc.git#egg=independoc
```

## Quickstart (Django)
Internally this project is primarily used for django applications. To get started, open up your project's *settings.py* file and:
1. Add *independoc* to your installed_apps:
	```python
	INSTALLED_APPS = [
		...
		'independoc',
	]
	```
2. Set the following config variables:
	```python
	INDEPENDOC_BACKEND = 'independoc.backends.NoBackend'
    INDEPENDOC_OUTPUT_DIR = './docs/documentation'
    INDEPENDOC_OUTPUT_URL = '/static/documentation/'
    INDEPENDOC_CONFIG = {}
	```
	(**NOTE:** Don't output to docs if you already have a *docs* folder in your django project)
	
3. Since we've told independoc to output to */docs/documentation* we need to ensure our static collector will pick it up:
	```python
    STATICFILES_DIRS = [
        ...
        os.path.join(BASE_DIR, 'docs'),
    ]
	```

Now you can run the following command to generate your static site:
```bash
python manage.py generate_independoc 
```

(**NOTE:** This command will *delete* the entire contents of *INDEPENDOC_OUTPUT_DIR* when called prior to building the static site.)

Use your standard collectstatic method to deploy.

Now your content will be available at "http://your_url/static/documentation/index.html" while the server is running.

This process will generate a basic empty site via the NoBackend Backend. To do anything more, read the *Backends* section below.

## Quickstart (Python Script)[Currently Unsupported]

If you *aren't* running Django, Independoc provides a script to do the same as above; however, you'll need to construct a JSON configuration file which looks like the following:
```
{
    "backend": "independoc.backends.NoBackend",
    "output_dir": "./doc/documentation",
    "output_url": "/documentation/",
    "configuration": {}
}
```

Then your site can be compiled with
```bash
independoc config.json
```

To view your site, either include it in a hosted static directory or run:
```
cd docs
# python2
python -m SimpleHTTPServer
# python3
python -m http.server
```
And browse to "http://localhost:8000/documentation/index.html".

This process will generate a basic empty site via the NoBackend Backend. Read the *Backends* section below to do anything more.

## Basic Configuration

The following standard information is expected in any configuration:
- **backend:**  module path to IndependocBackend subclass
- **output_dir:** directory to put contents in
- **output_url:** the root directory the content will be accessible at (used for generation)
- **configuration:** A dictionary / JSON object of backend-specific configuration


By default the *configuration* object takes the following **optional settings**. You'll likely only need to set site_title and index_contents_file for most websites.
- **site_title:** Title which will appear on navbar / index page
     - defaults to ```"Site Documentation"```
- **index_content_file:** Path to a file containing HTML to be placed on the index page
     - defaults to some basic html in the module
- **return_url:** Where the 'back to site' link goes
     - defaults to ```"/"```
- **template_package:** module that jinja will search for templates
    - defaults to ```"independoc"```.
    - not used if ```use_filesystemloader == True```
- **template_dir:** directory within the module containing the jinja templates
    - defaults to ```"/src/templates"```
    - uses absolute path if ```use_filesystemloader == True```
- **use_filesystemloader:** if you want to load templates from an absolute path instead of from a package, set this to true and set template_dir
   - defaults to False

Different backends will likely require several more configuration settings to function correctly.

## Backends

### NoBackend
This backend doesn't do anything except render the index_content_file.

An empty configuration dictionary is valid.

### GoogleDriveBackend
Downloads files from Google Drive. In order to configure, you will need to run the generation function once on a dev machine to generate your token file via a javascript-enabled browser.

Yes, this is terrible design on Google's part.

Follow **Step 1** from the [Google Drive REST API](https://developers.google.com/drive/api/v3/quickstart/python) to generate your credentials.json.

The following settings are **required** in the configuration dictionary:

- **credentials_file:** Path to file generated above (must end with .json)
- **token_file:** Where to store Google's generated token should be stored (must end in .json)
- **file_query:** search parameters for [Google Drive](https://developers.google.com/drive/api/v3/search-parameters) 

The easiest use case would be ```"'XXXXXXX' in parents and name contains 'pub_'"``` where XXXXXXX is replaced with the folder ID you'd like to search in (copied from the URL while browsing Google Drive).

## File-naming Convention
*It's a good idea to provide a copy of this information to your generous document writers.*

For files to be properly parsed, they need to be named certain ways.

All files which need to appear in the official documentation must start with ```pub_```, followed by ```<category>_```, and then the name of the file.

For instance, a video named "How to Get Started" should be named ```pub_video_How to Get Started.mp4```.

The following files are currently supported:

#### Videos
Video files must be .mp4 formatted and labelled as ```pub_video_FILENAME.mp4```.

If you have a document (such as a transcription) which needs to be included with the video, it should be named the same, but prefixed with ```pub_video-text_```

For example:
```
pub_video_My Video.mp4
pub_video-text_My Video
```

#### Articles

Simple name the file ```pub_article_FILENAME``. Embedded images will automatically be copied into the documentation.
