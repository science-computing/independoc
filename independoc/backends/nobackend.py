from independoc.backends.base import IndependocBackend


class NoBackend(IndependocBackend):
    required_config = []

    def get_files(self):
        return []
