from independoc.backends.base import IndependocBackend
from googleapiclient.discovery import build
from googleapiclient.http import MediaIoBaseDownload
from httplib2 import Http
from oauth2client import file, client, tools
from io import BytesIO

import re
import os
from bs4 import BeautifulSoup
import bleach
import requests


SCOPES = 'https://www.googleapis.com/auth/drive.readonly'


def downloadEmbeddedFile(url):
    fh = BytesIO()
    r = requests.get(url, stream=True)
    for chunk in r:
        fh.write(chunk)
    fh.seek(0)
    return fh

def downloadFile(service, gfile, mimetype=None):
    if mimetype is None:
        request = service.files().get_media(
            fileId=gfile['id']
        )
    else:
        request = service.files().export_media(
            fileId=gfile['id'],
            mimeType=mimetype
        )
    fh = BytesIO()
    downloader = MediaIoBaseDownload(fh, request)
    done = False
    while done is False:
        status, done = downloader.next_chunk()
    fh.seek(0)
    return (gfile['name'], fh)


def stripBody(service, gfile):
    n, f = downloadFile(service, gfile, 'text/html')
    content = BeautifulSoup(f.read(), 'html5lib').find('body')\
        .encode_contents().decode("utf-8")
    content = bleach.clean(
        content,
        tags=[
            'h1',
            'h2',
            'h3',
            'h4',
            'h5',
            'p',
            'ul',
            'ol',
            'li',
            'hr',
            'br',
            'strong',
            'em',
            'b',
            'i',
            'img',
            'a',
            'caption',
        ],
        attributes={
            'a': ['href'],
            'img': ['src', 'alt'],
            '*': ['id', 'class'],
        },
        strip=True
    )
    # Exctract the images and put them into IO files...
    # This is an abomination, we need to move it to it's own function
    # TODO: make this not so bad
    embedded_images = []
    r = re.compile(r'<img (.*?)src=\"((?:\S*?/)+(\S*?))\"', re.MULTILINE)
    for i, x in enumerate(r.finditer(content)):
        embedded_images.append(
            (x.group(3), downloadEmbeddedFile(x.group(2)))
        )
        content = content.replace(
            x.group(0),
            '<img {} src="{{{{resources[{}].url}}}}"'.format(x.group(1), i),
            1
        )
    return content, embedded_images
    return r.sub(
        lambda match: '<img {} src="{}{}"'
        .format(
            match.group(1),
            '/static/documentation/resouces',
            match.group(3).lower()
        ),
        content
    ), embedded_images
    return r.sub(
        r'<img \1 src="{}\3"'.format('/static/documentation/resources/'),
        content
    ), embedded_images


class GoogleDriveBackend(IndependocBackend):
    required_config = [
        'credentials_file',
        'token_file',
        'file_query',
    ]

    def get_files(self):
        # Connect to google drive
        store = file.Storage(self.configuration['token_file'])
        creds = store.get()
        if not creds or creds.invalid:
            flow = client.flow_from_clientsecrets(
                self.configuration['credentials_file'],
                SCOPES
            )
            flags = tools.argparser.parse_args(args=[])
            creds = tools.run_flow(flow, store, flags)
            creds = tools.run_flow(flow, store)
        service = build('drive', 'v3', http=creds.authorize(Http()))

        # Call the Drive v3 API
        results = service.files().list(
            pageSize=100,
            q=self.configuration['file_query'],
            fields="nextPageToken, files(id, name)"
        ).execute()
        results = results.get('files', [])
        for f in results:
            name = ''
            ext = ''
            category = None
            body = None
            files = []

            # Split things
            splits = f['name'].split('_', 2)
            if not len(splits) == 3:
                continue
            category = splits[1]
            name, ext = os.path.splitext(splits[-1])

            if category == 'video' and ext == '.mp4':
                # Download the video
                video = downloadFile(service, f)
                # Check for corresponding video-text
                # Bit lame, just checks for pub_video-text_<origname>
                # truncating the .mp4
                vtext_name = 'pub_video-text_{}'.format(
                    name
                )

                for c in results:
                    if c['name'] == vtext_name:
                        body, files = stripBody(service, c)
                        break

                files.append(video)

            elif (
                category != 'video' and
                category in self.collection.items.keys()
            ):
                body, files = stripBody(service, f)

            # If we didn't match the arms above, yield nothing
            else:
                continue

            yield((name, category, body, files))
