from jinja2 import Environment, PackageLoader, FileSystemLoader
import pkg_resources
import os
import shutil
import re
import unicodedata
from unidecode import unidecode

def autoescape(template_name):
    if template_name is None:
        return False
    if template_name.endswith(('.html', '.htm', '.xml')):
        return True


config_defaults = {
    'site_title': 'Site Documentation',
    'return_url': '/',
    'index_content_file': pkg_resources.resource_filename(
        'independoc', 'src/index_content.html'
    ),
    'template_package': 'independoc',
    'use_filesystemloader': False,
    'template_dir': '/src/templates',
}


def copytree(src, dst, symlinks=False, ignore=None):
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            shutil.copytree(s, d, symlinks, ignore)
        else:
            shutil.copy2(s, d)


class IndependocFile():
    def __init__(self, name, path, url, body, resources):
        self.name = name
        self.path = path
        self.url = url
        self.body = body
        self.resources = resources


# Just blocks or adds files
class IndependocCollection():
    categories = ['video', 'document', 'article']

    def __init__(
        self,
        backend_instance,
    ):
        self.backend = backend_instance
        self.items = {
            key: []
            for key in self.categories
        }

    def slugify_name(self, name, ext=True):
        if ext is True:
            name, ext = os.path.splitext(name)
        else:
            ext = ''
        return re.sub(
            r'[\W_]+',
            '-',
            unicodedata.normalize('NFKD', name).lower()
        ) + ext

    def add_file(self, name, category, body, files):
        resources = []
        for fname, io in files:
            fname = self.slugify_name(fname)
            path = os.path.join(self.backend.output_dir, 'resources', fname)
            url = self.backend.output_url + 'resources/' + fname
            with open(
                path, 'wb'
            ) as out:
                out.write(io.read())
            resources.append({'path': path, 'url': url})

        filename = self.slugify_name(name, ext=False) + '.html'
        path = os.path.join(self.backend.output_dir, category, filename)
        url = self.backend.output_url + category + '/' + filename
        n = IndependocFile(name, path, url, body, resources)
        self.items[category].append(n)

    def sort(self, key=lambda x: x.name):
        for k in self.items.keys():
            self.items[k].sort(key=key)


class IndependocBackend():
    required_config = []

    def __init__(self, output_dir, output_url, configuration):
        self.output_dir = output_dir
        self.output_url = output_url
        self.collection = IndependocCollection(self)
        self.configuration = dict()
        self.configuration.update(config_defaults)
        self.configuration.update(configuration)
        self.template_package = self.configuration.pop(
            'template_package'
        )
        self.template_dir = self.configuration.pop(
            'template_dir'
        )
        self.use_filesystemloader = self.configuration.pop(
            'use_filesystemloader'
        )

        for x in self.required_config:
            if x not in configuration:
                raise Exception((
                    'INDEPENDOC IMPRPOPERLY CONFIGURED: '
                    'Backend {} expected {}'
                ).format(self.__class__.__name__, x))

        self.prepare_output_dir()

    def prepare_output_dir(self):
        if os.path.isdir(self.output_dir):
            shutil.rmtree(self.output_dir)
        resources_dir = os.path.join(self.output_dir, 'resources')
        os.makedirs(resources_dir)
        static_resources = pkg_resources.resource_filename(
            'independoc', 'src/resources/'
        )
        copytree(static_resources, resources_dir)
        for cat in self.collection.items.keys():
            os.makedirs(os.path.join(self.output_dir, cat))

    def save_output_file(self, path, content):
        with open(path, 'w') as f:
            f.write(unidecode(content).encode('utf-8'))

    def collect_files(self):
        for f in self.get_files():
            self.collection.add_file(*f)
        self.collection.sort()

    def render_collection(self):
        env = Environment(
            loader=PackageLoader(self.template_package, self.template_dir)
            if not self.use_filesystemloader
            else FileSystemLoader(self.template_dir),
            autoescape=autoescape
        )

        with open(self.configuration['index_content_file']) as body:
            index_page = IndependocFile(
                'index',
                os.path.join(self.output_dir, 'index.html'),
                self.output_url,
                body.read(),
                []
            )
        for key, val in self.collection.items.items()\
                + [('index', [index_page])]:
            template = env.get_template('{}.html'.format(key))
            for page in val:
                # pre-compile body --
                # this hydrates resouces back in (if we replaced them earlier)
                if page.body is not None:
                    body = env.from_string(page.body)\
                        .render(resources=page.resources)
                else:
                    body = ' '
                self.save_output_file(
                    page.path,
                    template.render(
                        config=self.configuration,
                        name=page.name,
                        body=body,
                        url=page.url,
                        base_url=self.output_url,
                        resources=page.resources,
                        collection=self.collection.items
                    )
                )

    def get_files(self):
        # add files
        raise NotImplementedError()
