def build_static(backend, output_dir, output_url, configuration):
    backend = backend(output_dir, output_url, configuration)
    backend.collect_files()
    backend.render_collection()
