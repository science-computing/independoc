from django.core.management.base import BaseCommand, CommandError
from independoc import build_static
from django.conf import settings
import importlib


class Command(BaseCommand):
    help = 'Generates Documentation!'

    def add_arguments(self, parser):
        parser.add_argument('args', nargs='*')

    def handle(self, *args, **options):
        self.stdout.write("Generating Documentation")
        backend = settings.INDEPENDOC_BACKEND
        module = '.'.join(backend.split('.')[:-1])
        backend = backend.split('.')[-1]
        module = importlib.import_module(module)
        backend = getattr(module, backend)
        build_static(
            backend,
            settings.INDEPENDOC_OUTPUT_DIR,
            settings.INDEPENDOC_OUTPUT_URL,
            getattr(settings, 'INDEPENDOC_CONFIG', {}),
        )
